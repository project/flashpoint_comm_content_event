<?php


namespace Drupal\flashpoint_comm_content_event\Breadcrumb;


use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\group\Entity\GroupContent;


class FlashpointCommunityContentEventBreadcrumbBuilder implements BreadcrumbBuilderInterface {
  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $params = $route_match->getParameters()->all();
    if (isset($params['flashpoint_community_content']) && !empty($params['flashpoint_community_content'])) {
      /* @var $flashpoint_community_content \Drupal\flashpoint_community_content\Entity\FlashpointCommunityContent */
      $flashpoint_community_content = $params['flashpoint_community_content'];

      /*
       * This breadcrumb applies to event content.
       */
      if ($flashpoint_community_content->bundle() === 'event') {
        return TRUE;
      }
    }
  }


  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $breadcrumb = new Breadcrumb();
    $url = Url::fromRoute('<front>');
    $breadcrumb->addLink(Link::fromTextAndUrl('Home', $url));

    /* @var $flashpoint_community_content \Drupal\flashpoint_community_content\Entity\FlashpointCommunityContent */
    $flashpoint_community_content = \Drupal::routeMatch()->getParameter('flashpoint_community_content');

    $gcs = GroupContent::loadByEntity($flashpoint_community_content);
    if (!empty($gcs)) {
      $gc = array_shift($gcs);
      /* @var $gc \Drupal\group\Entity\GroupContent */
      $group = $gc->getGroup();
      $link = $group->toLink($group->label(), 'canonical');
      $breadcrumb->addLink($link);

      $e_url = Url::fromRoute('view.flashpoint_community_events_in_community.page_1', ['group' => $group->id()]);
      $e_link = Link::fromTextAndUrl('Events', $e_url);
      $breadcrumb->addLink($e_link);

      $breadcrumb->addCacheContexts(['route']);
      return $breadcrumb;
    }
    else {
      $breadcrumb->addCacheContexts(['route']);
      return $breadcrumb;
    }
  }
}